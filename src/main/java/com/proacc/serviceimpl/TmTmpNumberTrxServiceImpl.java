package com.proacc.serviceimpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.repository.TmTmpNumberTrxRepository;
import com.proacc.repository.TmConfigRepository;
import com.proacc.serializable.TMConfigSerializable;
import com.proacc.serializable.TmTmpNumberSerializable;
import com.proacc.entity.TmConfig;
import com.proacc.entity.TmTmpNumberTrx;
import com.proacc.service.TmTmpNumberTrxService;

@Service
public class TmTmpNumberTrxServiceImpl implements TmTmpNumberTrxService {
	
	@Autowired
	private TmTmpNumberTrxRepository numRepo;

	@Override
	public Optional<TmTmpNumberTrx> getTmpNumberTrx(String trxName, int companyId) {
		// TODO Auto-generated method stub
		return numRepo.findByTrxNameAndCompanyId(trxName, companyId);
	}

	@Override
	public Optional<TmTmpNumberTrx> findId(int id, int company) {
		// TODO Auto-generated method stub
		return numRepo.findById(new TmTmpNumberSerializable(id, company));
	}

	@Override
	public void save(TmTmpNumberTrx paramTmp) {
		// TODO Auto-generated method stub
		numRepo.save(paramTmp);

	}



}
