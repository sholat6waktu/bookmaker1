package com.proacc.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
@Configuration
public class AccessCodeServiceInterceptorConfig extends WebMvcConfigurerAdapter{
	@Autowired
	AccessCodeServiceInterceptor accessCodeServiceInterceptor;
	
	@Override
   public void addInterceptors(InterceptorRegistry registry) {
      registry.addInterceptor(accessCodeServiceInterceptor);
   }
	
}
