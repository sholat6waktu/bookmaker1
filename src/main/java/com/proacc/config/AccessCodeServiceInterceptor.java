package com.proacc.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import net.sf.json.JSONObject;

@Component
public class AccessCodeServiceInterceptor implements HandlerInterceptor {
	 @Override
	 public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		 String userAccess = request.getHeader("User-Access");
		 JSONObject resp = new JSONObject();
		 JSONObject dataRespAccess = Constants.checkProAccess(userAccess);
		 System.out.println("Pre Handle method is Calling");
		 if ((boolean) dataRespAccess.get("status")) {
			 return true;
		 }else {
			 resp.put("status", "01");
			 resp.put("message", dataRespAccess.get("message"));
			 response.getWriter().write(resp.toString());
			 return false;
		 }
	 }
	 
	 
}
