package com.proacc.service;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public interface TbTrainingService {
	List<List<Object[]>> getallbaru(String customer, String company, String periode1, String periode2, String periode3, String periode4, String periode5);
}
