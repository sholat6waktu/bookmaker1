package com.proacc.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.TmTmpNumberTrx;
import com.proacc.serializable.TmTmpNumberSerializable;
public interface TmTmpNumberTrxRepository extends JpaRepository<TmTmpNumberTrx, TmTmpNumberSerializable>{
	public Optional<TmTmpNumberTrx> findByTrxNameAndCompanyId(final String trxName, final int companyId);
}


