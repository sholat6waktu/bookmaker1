package com.proacc.serializable;

import javax.persistence.Embeddable;
import javax.persistence.Column;
import java.io.Serializable;

@Embeddable
public class TMConfigSerializable implements Serializable {
	@Column(name = "id")
	public int id;
	
	@Column(name = "company_code")
	public Integer companyCode;
	
	public TMConfigSerializable() {}
	
	public TMConfigSerializable(int id, Integer companyCode) 
	{
		this.id = id;
		this.companyCode = companyCode;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getCompanyId() {
		return companyCode;
	}

	public void setCompanyId(Integer companyCode) {
		this.companyCode = companyCode;
	}
	
	
}
